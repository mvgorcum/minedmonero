# Mined Monero Android App
Written by R. Harkes and M. van Gorcum.

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png"  height="80">](https://play.google.com/store/apps/details?id=com.vgorcum.minedmonero)
[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/packages/com.vgorcum.minedmonero/)

This app is written for monero miners participating in a supported pool.  
You can use it to track how much you've mined, how much has been paid out and 
how much is still due, both in XMR and a fiat currency of choice.

![Screenshot stats][statshot] ![Screenshot settings][setshot]

[statshot]: https://gitlab.com/mvgorcum/minedmonero/raw/master/Screenshot_Stats.png
[setshot]: https://gitlab.com/mvgorcum/minedmonero/raw/master/Screenshot_Settings.png