package com.vgorcum.minedmonero;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Resources res = getResources();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        int fiatpos = sharedPref.getInt("FiatPos",1);
        String[] fiatarray = res.getStringArray(R.array.currency_array);
        String[] fiatspinner = new String[fiatarray.length];
        for (int i=0; i<fiatarray.length;i++){
            String[] tmp = fiatarray[i].split(",");
            fiatspinner[i]=tmp[0];
        }
        Spinner fiatselect = findViewById(R.id.Fiatselect);
        ArrayAdapter<String> fiatadapter= new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, fiatspinner);
        fiatadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fiatselect.setAdapter(fiatadapter);
        fiatselect.setSelection(fiatpos);

        int poolpos = sharedPref.getInt("PoolPos",0);
        Spinner poolselect = findViewById(R.id.Poolselect);
        ArrayAdapter<CharSequence> pooladapter = ArrayAdapter.createFromResource(this,
                R.array.pool_array, android.R.layout.simple_spinner_item);
        pooladapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        poolselect.setAdapter(pooladapter);
        poolselect.setSelection(poolpos);

        String WalletAdd = sharedPref.getString("WalletAddress",null);
        EditText editText =  findViewById(R.id.InputWallet);
        editText.setText(WalletAdd, TextView.BufferType.EDITABLE);

    }
    public void ScanQR(View view) {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (Exception e) {
            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
            startActivity(marketIntent);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                EditText editText =  findViewById(R.id.InputWallet);
                editText.setText(contents.substring(contents.indexOf("4")));
            }
            if(resultCode == RESULT_CANCELED){
                EditText editText =  findViewById(R.id.InputWallet);
                editText.setError("QR scan canceled");
            }
        }
    }
    public void InputAddress(View view) {
        Intent intent = new Intent(this, DisplayCurrentValue.class);
        EditText editText =  findViewById(R.id.InputWallet);
        Wallet awallet = new Wallet(editText.getText().toString());
        if (awallet.valid) {
            Spinner poolselect = (Spinner) findViewById(R.id.Poolselect);
            String pool = poolselect.getSelectedItem().toString();
            int poolpos = poolselect.getSelectedItemPosition();

            Spinner fiatselect = (Spinner) findViewById(R.id.Fiatselect);
            String fiat = fiatselect.getSelectedItem().toString();
            int fiatpos = fiatselect.getSelectedItemPosition();

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("WalletAddress", awallet.GetString());
            editor.putString("Pool", pool);
            editor.putInt("PoolPos", poolpos);
            editor.putString("Currency", fiat);
            editor.putInt("FiatPos", fiatpos);
            editor.apply();
            startActivity(intent);
        }else{
            editText.setError(awallet.GetError());
            }
    }
}
