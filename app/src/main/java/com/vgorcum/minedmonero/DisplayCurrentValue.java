package com.vgorcum.minedmonero;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Locale;

public class DisplayCurrentValue extends AppCompatActivity {
    RequestQueue requestQueue;
    TextView CurrentPrice;
    TextView dispMinedamt;
    TextView dispMinedamtEur;
    TextView dispHashrate;
    TextView dispPaidamt;
    TextView dispPaidamtEur;
    double EurPriceVar;
    double Minedamt;
    double Paidamt;
    String url;

    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    // values that atm are static but might be set by configuration in the future
    static String baseURLprice = "https://api.coinmarketcap.com/v1/ticker/";
    static String baseURLmined;
    static String[] Currency = new String[3];
    static String[] Altcoin = {"XMR","monero"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_current_value);
        mDrawerList = findViewById(R.id.navList);
        addDrawerItems();
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        Resources res = getResources();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        if (sharedPref.getString("WalletAddress",null)==null){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        int fiatpos = sharedPref.getInt("FiatPos",1);
        String[] fiatarray = res.getStringArray(R.array.currency_array);
        Currency = fiatarray[fiatpos].split(",");

        requestQueue = Volley.newRequestQueue(this);
        String pool =sharedPref.getString("Pool","supportxmr.com");
        baseURLmined="https://" + pool + "/api/miner/";

        //link to GUI fields
        this.CurrentPrice = findViewById(R.id.CurrentPrice);
        this.dispMinedamtEur = findViewById(R.id.euramtdue);
        this.dispMinedamt = findViewById(R.id.amtdue);
        this.dispHashrate = findViewById(R.id.hashrate);
        this.dispPaidamt = findViewById(R.id.paidamt);
        this.dispPaidamtEur = findViewById(R.id.paidamteur);
        refreshData();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        if (position==0) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        else if (position==1){
            Intent intent = new Intent(this, DisplayCurrentValue.class);
            startActivity(intent);

        }

    }

    private void addDrawerItems() {
        String[] osArray = {"Settings", "Mined Amount"};
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);
    }

    private void setEurPriceText(String str) {
        EurPriceVar=Double.parseDouble(str);
        this.CurrentPrice.setText(String.format(Locale.US,"%s%.2f",Currency[1],EurPriceVar));
        setMinedEUR();
    }

    private void setMinedamt(Long mined, Long amtpaid, String currhashrate) {

        Minedamt = mined * 0.000000000001;
        Paidamt = amtpaid * 0.000000000001;
        this.dispMinedamt.setText(String.format(Locale.US,"%.4f %s",Minedamt,Altcoin[0]));
        this.dispPaidamt.setText(String.format(Locale.US,"%.4f %s",Paidamt,Altcoin[0]));
        this.dispHashrate.setText(currhashrate);

        setMinedEUR();
    }

    private void setMinedEUR(){
        double Eurmined=EurPriceVar*Minedamt;
        double Eurpaid=EurPriceVar*Paidamt;
        this.dispMinedamtEur.setText(String.format(Locale.US,"%s%.2f",Currency[1],Eurmined));
        this.dispPaidamtEur.setText(String.format(Locale.US,"%s%.2f",Currency[1],Eurpaid));
    }

    private void getCurrentPrice(String altcoinName,String currency) {
        this.url = baseURLprice + altcoinName + "/?convert=" + currency;
        JsonArrayRequest arrReqprice = new JsonArrayRequest(Request.Method.GET, url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (response.length() > 0) {
                            try {
                                JSONObject jsonObj = response.getJSONObject(0);
                                setEurPriceText(jsonObj.getString(Currency[2]));
                            } catch (JSONException e) {
                                Log.w("Volley", "Invalid JSON Object.");
                                Log.w("URLaccessed",url);
                                Context context = getApplicationContext();
                                CharSequence text = "Error connecting to coinmarketcap";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }
                        }else{
                            String priceEur = "api call error";
                            setEurPriceText(priceEur);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Context context = getApplicationContext();
                        CharSequence text = "Error connecting to coinmarketcap";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        Log.w("URLaccessed",url);
                        Log.w("Volley", error.toString());
                    }
                }
        );
        requestQueue.add(arrReqprice);
    }

    private void getMined(String WalletAddress) {
        this.url = baseURLmined + WalletAddress + "/stats";
        JsonObjectRequest arrReqmined = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.length() > 0) {
                            try {
                                Long mined = response.getLong("amtDue");
                                Long amtpaid = response.getLong("amtPaid");
                                String currhashrate = response.getString("hash");

                                setMinedamt(mined, amtpaid, currhashrate);
                            } catch (JSONException e) {
                                Log.w("Volley", "Invalid JSON Object.");
                                Log.w("URLaccessed",url);
                                Context context = getApplicationContext();
                                CharSequence text = "Error connecting to mining pool";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }

                        }else{
                            Log.w("myapp","into else");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Context context = getApplicationContext();
                        CharSequence text = "Error connecting to mining pool";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                        Log.w("Volley", error.toString());
                        Log.w("URLaccessed",url);
                    }
                }
        );
        requestQueue.add(arrReqmined);
    }

    public void refreshClicked(View v) {
        refreshData();
    } //might be moved to the GUI itself

    public void refreshData(){
        getCurrentPrice(Altcoin[1],Currency[0]);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String wallet = sharedPref.getString("WalletAddress",null);
        getMined(wallet);
    }
}
